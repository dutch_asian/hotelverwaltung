/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotelverwaltung;

import data.Benutzer;
import data.Buchung;
import data.Kunde;
import data.Leistung;
import data.Position;
import data.Rechnung;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author 5im13allau
 */
public class Hotelverwaltung {

    Document document;

    public Hotelverwaltung() {

    }

    public int generateRandomInt(int min, int max) {
        Random random = new Random();
        return random.nextInt(max - min + 1) + min;
    }

    public Date addRandomDateToDate(Date toAdd) {
        Date thisDate = toAdd;
        Calendar c = Calendar.getInstance();
        c.setTime(thisDate);
        int rand = generateRandomInt(1, 10);
        c.add(Calendar.DATE, rand);
        thisDate = c.getTime();

        return thisDate;
    }

    public Date generateDateBetween(Date begin, Date end) {
        long MILLIS_PER_DAY = 1000 * 60 * 60 * 24;
        GregorianCalendar s = new GregorianCalendar();
        s.setTimeInMillis(begin.getTime());
        GregorianCalendar e = new GregorianCalendar();
        e.setTimeInMillis(end.getTime());

        long endL = e.getTimeInMillis() + e.getTimeZone().getOffset(e.getTimeInMillis());
        long startL = s.getTimeInMillis() + s.getTimeZone().getOffset(s.getTimeInMillis());
        long dayDiff = (endL - startL) / MILLIS_PER_DAY;

        Calendar cal = Calendar.getInstance();
        cal.setTime(begin);
        cal.add(Calendar.DATE, ThreadLocalRandom.current().nextInt((int) dayDiff + 1));
        return cal.getTime();
    }

    public Buchung enterBuchung(Kunde kunde, Date anfang, Date ende) {
        Buchung b = new Buchung();

        b.setKunde(kunde);
        b.setAnkunft(anfang);
        b.setAbreise(ende);

        return b;
    }

    public Position enterPosition(Buchung buchung, Leistung leistung, Benutzer benutzer, int anzahl, BigDecimal preis, BigDecimal rabatt, Date date) {
        Position p = new Position();

        p.setBuchung(buchung);
        p.setLeistung(leistung);
        p.setBenutzer(benutzer);
        p.setAnzahl(anzahl);
        p.setPreis(preis);
        p.setRabatt(rabatt);
        p.setDatum(date);

        return p;
    }

    public void generateRandomBuchungen() throws ParseException {
        Session session = HibernateUtil.getInstance().openSession();
        Transaction transaction = session.beginTransaction();
        DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        Date dateAYearAgo = format.parse("11-05-2015");
        Date todayAYear = format.parse("11-05-2017");
        Random random = new Random();
        List<Kunde> kunden = session.createQuery("FROM Kunde").list();

        for (Kunde k : kunden) {
            session.save(k);
            Date dateBegin = generateDateBetween(dateAYearAgo, todayAYear);
            Date dateEnde = addRandomDateToDate(dateBegin);
            session.save(enterBuchung(k, dateBegin, dateEnde));
        }
        transaction.commit();
        session.close();
    }

    public void generateRandomPosition() throws ParseException {
        Session session = HibernateUtil.getInstance().openSession();
        Transaction tr = session.beginTransaction();
        List<Buchung> buchungen = session.createQuery("FROM Buchung").list();
        List<Benutzer> benutzer = session.createQuery("FROM Benutzer").list();
        List<Leistung> leistungen = session.createQuery("FROM Leistung").list();

        DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        Date dateToday = format.parse("25-05-2016");

        for (Buchung b : buchungen) {
            if (b.getAbreise().before(dateToday)) {
                int randomL = generateRandomInt(0, leistungen.size() - 1);
                double rabatt = generateRandomInt(0, 20);
                rabatt = rabatt / 100;
                int anzahl = generateRandomInt(1, 5);
                double price = anzahl * leistungen.get(randomL).getPreis();
                double rabattPrice = rabatt * price;

                session.save(enterPosition(b,
                        leistungen.get(randomL),
                        benutzer.get(generateRandomInt(0, benutzer.size() - 1)),
                        anzahl,
                        new BigDecimal(price),
                        new BigDecimal(rabattPrice),
                        generateDateBetween(b.getAnkunft(), b.getAbreise())));
            }
        }
        tr.commit();
        session.close();
    }

    public Rechnung enterRechnung(BigDecimal betrag, Date bis, Date am, Buchung buchung) {
        Rechnung r = new Rechnung();

        r.setBetrag(betrag);
        r.setBezahlenBis(bis);
        r.setBezahltAm(am);
        r.setBuchung(buchung);

        return r;
    }

    public double getCosts(Buchung b) {
        double preis = 0;

        for (Position p : b.getPositions()) {
            preis = preis + (p.getPreis().doubleValue() - p.getRabatt().doubleValue());
        }

        //preis = preis + preis;

        return preis;
    }

    public void generateRandomRechnung() throws ParseException {
        Session session = HibernateUtil.getInstance().openSession();
        Transaction tr = session.beginTransaction();
        List<Buchung> buchungen = session.createQuery("FROM Buchung").list();
        DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        Date dateToday = format.parse("16-05-2016");
        Date bezahlenBis = new Date();

        for (Buchung b : buchungen) {
            if (b.getAbreise().before(dateToday)) {
                Calendar c = Calendar.getInstance();
                c.setTime(b.getAbreise());
                c.add(Calendar.DATE, 30);
                bezahlenBis = c.getTime();

                session.save(enterRechnung(new BigDecimal(getCosts(b)), bezahlenBis, generateDateBetween(b.getAbreise(), bezahlenBis), b));
            }
        }
        tr.commit();
        session.close();
    }

    public String getPDFInfo() {

        try {
            Rechnung rechnung = new Rechnung();
//        String betrag = "default";
//        String bezahlenBis = "default";
//        String bezahltAm = "default";
//        String kundenname = "default";
//        String datumBeginn = "default";
//        String datumEnde = "default";
//        String leistung = "default";
//        String benutzer = "default";
//        String anzahl = "default";
//        String preis = "default";
//        String rabatt = "default";
//        String datum = "default";
            DateFormat format = new SimpleDateFormat("dd-MM-yyyy");

            Session session = HibernateUtil.getInstance().openSession();
            Transaction tr = session.beginTransaction();

            List<Rechnung> rechnungen = session.createQuery("FROM Rechnung").list();
            List<Position> positionen = session.createQuery("FROM Position").list();
            List<Buchung> buchungen = session.createQuery("FROM Buchung").list();

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            document = builder.newDocument();
            Element rootElement = document.createElement("rechnung");
            document.appendChild(rootElement);
            System.out.println("Es wurde getPDFInfo aufgerufen mit diesem parameter: ");

            rechnung = rechnungen.get(generateRandomInt(0, rechnungen.size() - 1));

            Element betr = document.createElement("betrag");
            betr.setTextContent(rechnung.getBetrag().toString());
            rootElement.appendChild(betr);

            Element bzbis = document.createElement("bezahlen_bis");
            bzbis.setTextContent(format.format(rechnung.getBezahlenBis()));
            rootElement.appendChild(bzbis);

            Element bzam = document.createElement("bezahlt_am");
            bzam.setTextContent(format.format(rechnung.getBezahltAm()));
            rootElement.appendChild(bzam);

            Element kname = document.createElement("kundenname");
            kname.setTextContent(rechnung.getBuchung().getKunde().getVorname() + " " + rechnung.getBuchung().getKunde().getName());
            rootElement.appendChild(kname);

            Element ankunft = document.createElement("ankunft");
            ankunft.setTextContent(format.format(rechnung.getBuchung().getAnkunft()));
            rootElement.appendChild(ankunft);

            Element abreise = document.createElement("abreise");
            abreise.setTextContent(format.format(rechnung.getBuchung().getAbreise()));
            rootElement.appendChild(abreise);

            for (Position p : rechnung.getBuchung().getPositions()) {

                Element pos = document.createElement("position");
                rootElement.appendChild(pos);

                Element leistung = document.createElement("leistung");
                leistung.setTextContent(p.getLeistung().getBeschreibung());
                pos.appendChild(leistung);

                Element benutzer = document.createElement("benutzer");
                benutzer.setTextContent(p.getBenutzer().getBenutzer());
                pos.appendChild(benutzer);

                Element anzahl = document.createElement("anzahl");
                anzahl.setTextContent(p.getAnzahl().toString());
                pos.appendChild(anzahl);

                Element preis = document.createElement("preis");
                preis.setTextContent(p.getPreis().toString());
                pos.appendChild(preis);

                Element rabatt = document.createElement("rabatt");
                rabatt.setTextContent(p.getRabatt().toString());
                pos.appendChild(rabatt);

                Element datum = document.createElement("datum");
                datum.setTextContent(format.format(p.getDatum()));
                pos.appendChild(datum);
            }

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(document);
            DateFormat dateformat = new SimpleDateFormat("dd-MM-yyyy");
            Date date = new Date();
            StreamResult result = new StreamResult(new File("D:\\getPDFInfo" + dateformat.format(date) + ".xml"));
            transformer.transform(source, result);

            StringWriter outWriter = new StringWriter();
            StreamResult resulte = new StreamResult(outWriter);

            transformer.transform(source, resulte);
            StringBuffer sb = outWriter.getBuffer();
            String finalstring = sb.toString();

            session.close();

            return finalstring;

        } catch (TransformerConfigurationException ex) {
            Logger.getLogger(Hotelverwaltung.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TransformerException ex) {
            Logger.getLogger(Hotelverwaltung.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(Hotelverwaltung.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    public static void main(String[] args) throws ParseException {
        Hotelverwaltung hv = new Hotelverwaltung();
        //hv.generateRandomBuchungen();
        //hv.generateRandomPosition();
        System.out.println(hv.getPDFInfo());
        //hv.generateRandomRechnung();
        //hv.enterBuchung(new Kunde("Jorrit", "vdz"), "16-05-2016", "18-05-2016");
        //hv.enterBuchung(new Kunde("Alex1", "Lau2"), "18-05-2016", "20-05-2016");
    }

}
