/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotelverwaltung;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author 5im13allau
 */
public class HibernateUtil {

    private static HibernateUtil instance = null;
    private SessionFactory factory;

    private HibernateUtil() {
        Configuration configuration = new Configuration();
        configuration.configure("Hibernate/hibernate.cfg.xml");
        StandardServiceRegistryBuilder ssrb
                = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
        factory = configuration.buildSessionFactory(ssrb.build());
    }

    public static HibernateUtil getInstance() {
        if (instance == null) {
            instance = new HibernateUtil();
        }
        return instance;
    }

    public Session openSession() {
        return factory.openSession();
    }

    public void cleanUp() {
        factory.close();
    }
}
