package data;
// Generated 25.05.2016 16:11:17 by Hibernate Tools 4.3.1

import java.util.HashSet;
import java.util.Set;

/**
 * Leistung generated by hbm2java
 */
public class Leistung implements java.io.Serializable {

    private Integer leistungId;
    private String beschreibung;
    private Double preis;
    private Set<Position> positions = new HashSet<Position>(0);

    public Leistung() {
    }

    public Leistung(String beschreibung, Double preis, Set<Position> positions) {
        this.beschreibung = beschreibung;
        this.preis = preis;
        this.positions = positions;
    }

    public Integer getLeistungId() {
        return this.leistungId;
    }

    public void setLeistungId(Integer leistungId) {
        this.leistungId = leistungId;
    }

    public String getBeschreibung() {
        return this.beschreibung;
    }

    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }

    public Double getPreis() {
        return this.preis;
    }

    public void setPreis(Double preis) {
        this.preis = preis;
    }

    public Set<Position> getPositions() {
        return this.positions;
    }

    public void setPositions(Set<Position> positions) {
        this.positions = positions;
    }

}
