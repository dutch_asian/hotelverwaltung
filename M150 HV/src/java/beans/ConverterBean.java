/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.*;

/**
 *
 * @author 5im13pamueller
 */
public class ConverterBean {

    private final StreamSource source;
    private final StreamSource stylesheet;

    public ConverterBean(String xmlsource) {
        InputStream stream = new ByteArrayInputStream(xmlsource.getBytes(StandardCharsets.UTF_8));
        source = new StreamSource(stream);
        stylesheet = new StreamSource(new File("C:\\Users\\Jorrit van der Zee\\Desktop\\hotelverwaltungeeeeeeeeee\\M150 HV\\src\\java\\beans\\stylesheet_rechnung_pdf.xsl"));
    }

    public void createPDF(OutputStream target) throws Exception {
        Fop fop = FopFactory.newInstance().newFop(MimeConstants.MIME_PDF, target);
        Result sax = new SAXResult(fop.getDefaultHandler());
        Transformer transformer = TransformerFactory.newInstance().newTransformer(stylesheet);
        transformer.transform(source, sax);
        target.close();
    }

    public static void main(String[] args) {
        ConverterBean pc = new ConverterBean("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                + "<rechnung xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"Rechnung_pdf.xsd\">\n"
                + "<betrag>239.00</betrag>\n"
                + "<bezahlen_bis>17-10-2015</bezahlen_bis>\n"
                + "<bezahlt_am>10-10-2015</bezahlt_am>\n"
                + "<kundenname>Matthias Landolt</kundenname>\n"
                + "<ankunft>10-09-2015</ankunft>\n"
                + "<abreise>17-09-2015</abreise>\n"
                + "<position>\n"
                + "<leistung>Telefon&#13;</leistung>\n"
                + "<benutzer>Heinz</benutzer>\n"
                + "<anzahl>4</anzahl>\n"
                + "<preis>40.00</preis>\n"
                + "<rabatt>6.00</rabatt>\n"
                + "<datum>17-09-2015</datum>\n"
                + "</position>\n"
                + "<position>\n"
                + "<leistung>Freizeitangebot&#13;</leistung>\n"
                + "<benutzer>Benjamin</benutzer>\n"
                + "<anzahl>5</anzahl>\n"
                + "<preis>250.00</preis>\n"
                + "<rabatt>45.00</rabatt>\n"
                + "<datum>15-09-2015</datum>\n"
                + "</position>"
                + "</rechnung>");
        OutputStream out;
        try {
            out = new FileOutputStream("web/test.pdf");
            out = new BufferedOutputStream(out);
            pc.createPDF(out);
        } catch (Exception e) {
            System.err.println("Could not write file!");
        }
    }

}
