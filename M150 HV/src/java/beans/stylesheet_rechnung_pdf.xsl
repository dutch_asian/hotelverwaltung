<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions">
    <xsl:output encoding="UTF-8" indent="yes" method="xml" standalone="no" omit-xml-declaration="no"/>
    <xsl:template match="rechnung">
        <fo:root language="DE">
            <fo:layout-master-set>
                <fo:simple-page-master master-name="A4-main" page-height="297mm" page-width="210mm" margin-top="5mm" margin-bottom="5mm" margin-left="5mm" margin-right="5mm">
                    <fo:region-body margin-top="11mm" margin-bottom="11mm"/>
                    <fo:region-before extent="10mm"/>
                    <fo:region-after extent="10mm"/>
                </fo:simple-page-master>
            </fo:layout-master-set>
            <fo:page-sequence master-reference="A4-main">
                <fo:flow flow-name="xsl-region-body" border-collapse="collapse" reference-orientation="0">
                    <fo:block font-size="200%">
                        Rechnung
                    </fo:block>
                    <fo:block>
                        Totalbetrag: <xsl:value-of select="betrag"/>
                    </fo:block>
                    <fo:block>
                        Zu bezahlen bis: <xsl:value-of select="bezahlen_bis"/>
                    </fo:block>
                    <fo:block>
                        Bezahlt am: <xsl:value-of select="bezahlt_am"/>
                    </fo:block>
                    <fo:block>
                        Kunde: <xsl:value-of select="kundenname"/>
                    </fo:block>
                    <fo:block>
                        Ankunft: <xsl:value-of select="ankunft"/>
                    </fo:block>
                    <fo:block>
                        Abreise: <xsl:value-of select="abreise"/>
                    </fo:block>
                    
                    
                    <fo:block font-size="150%">Leistungen</fo:block>
					<fo:table table-layout="fixed" width="100%" font-size="10pt" border-color="black" border-width="0.35mm" border-style="solid" text-align="center" display-align="center" space-after="5mm">
						<fo:table-column column-width="proportional-column-width(25)"/>
						<fo:table-column column-width="proportional-column-width(25)"/>
						<fo:table-column column-width="proportional-column-width(25)"/>
                                                <fo:table-column column-width="proportional-column-width(25)"/>
                                                <fo:table-column column-width="proportional-column-width(25)"/>
						<fo:table-column column-width="proportional-column-width(25)"/>
						<fo:table-body font-size="95%">
							<fo:table-row height="8mm">
								<fo:table-cell>
									<fo:block>Leistung</fo:block>
								</fo:table-cell>
								<fo:table-cell>
									<fo:block>Mitarbeiter</fo:block>
								</fo:table-cell>
								<fo:table-cell>
									<fo:block>Anzahl</fo:block>
								</fo:table-cell>
								<fo:table-cell>
									<fo:block>Preis</fo:block>
								</fo:table-cell>
                                                                <fo:table-cell>
									<fo:block>Rabatt</fo:block>
								</fo:table-cell>
                                                                <fo:table-cell>
									<fo:block>Datum</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<xsl:for-each select="position">
								<fo:table-row>
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="leistung"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="benutzer"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="anzahl"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="preis"/>
										</fo:block>
									</fo:table-cell>
                                                                        <fo:table-cell>
										<fo:block>
											<xsl:value-of select="rabatt"/>
										</fo:block>
									</fo:table-cell>
                                                                        <fo:table-cell>
										<fo:block>
											<xsl:value-of select="datum"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:for-each>
						</fo:table-body>
					</fo:table>
                    
                    
                    
<!--                    <fo:table table-layout="fixed" width="100%" font-size="10pt" border-color="black" border-width="0.35mm" border-style="solid" text-align="left" display-align="center" space-after="5mm">
                        <fo:table-column column-width="proportional-column-width(100)"/>
                        <fo:table-body font-size="95%">
                            <fo:table-row height="8mm">
                                <fo:table-cell>
                                    <fo:block>Leistungen</fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                            <xsl:for-each select="position">
                                <fo:table-row>
                                    <fo:table-cell>
                                        <fo:block>
                                            Leistung: <xsl:value-of select="leistung"/>
                                        </fo:block>
                                    
                                        <fo:block>
                                            Mitarbeiter: <xsl:value-of select="benutzer"/>
                                        </fo:block>
                                    
                                        <fo:block>
                                            Anzahl: <xsl:value-of select="anzahl"/>
                                        </fo:block>
                                    
                                        <fo:block>
                                            Preis: <xsl:value-of select="preis"/>
                                        </fo:block>
                                    
                                        <fo:block>
                                            Rabatt: <xsl:value-of select="rabatt"/>
                                        </fo:block>
                                    
                                        <fo:block>
                                            Datum: <xsl:value-of select="datum"/>
                                        </fo:block>
                                    </fo:table-cell>
                                </fo:table-row>
                            </xsl:for-each>
                        </fo:table-body>
                    </fo:table>-->
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>
</xsl:stylesheet>
