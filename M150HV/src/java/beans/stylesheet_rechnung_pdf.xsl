<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions">
    <xsl:output encoding="UTF-8" indent="yes" method="xml" standalone="no" omit-xml-declaration="no"/>
    <xsl:template match="rechnung">
        <fo:root language="DE">
            <fo:layout-master-set>
                <fo:simple-page-master master-name="A4-main" page-height="297mm" page-width="210mm" margin-top="5mm" margin-bottom="5mm" margin-left="5mm" margin-right="5mm">
                    <fo:region-body margin-top="11mm" margin-bottom="11mm"/>
                    <fo:region-before extent="10mm"/>
                    <fo:region-after extent="10mm"/>
                </fo:simple-page-master>
            </fo:layout-master-set>
            <fo:page-sequence master-reference="A4-main">
                <fo:flow flow-name="xsl-region-body" border-collapse="collapse" reference-orientation="0">
				   <fo:block>
                                        <fo:instream-foreign-object>
					<svg version="1.0" xmlns="http://www.w3.org/2000/svg"
 width="300.000000pt" height="113.000000pt" viewBox="0 0 300.000000 113.000000"
 preserveAspectRatio="xMidYMid meet">
<metadata>
Created by potrace 1.10, written by Peter Selinger 2001-2011
</metadata>
<g transform="translate(0.000000,113.000000) scale(0.050000,-0.050000)"
fill="#000000" stroke="none">
<path d="M871 1869 c-42 -43 -39 -104 8 -133 26 -16 36 -40 30 -75 -11 -77
-27 -283 -42 -556 -7 -135 -19 -245 -26 -245 -35 0 -216 764 -195 820 26 69
-7 120 -76 120 -92 0 -121 -141 -35 -169 30 -9 43 -68 81 -356 26 -190 51
-376 57 -415 7 -50 -36 14 -152 229 -133 243 -160 309 -150 355 15 68 -18 116
-79 116 -101 0 -128 -120 -37 -168 49 -25 71 -76 193 -445 172 -524 191 -546
472 -546 325 -1 346 22 506 567 103 353 136 442 168 454 76 28 78 120 4 159
-68 36 -124 -12 -113 -98 7 -57 -15 -128 -100 -323 -60 -137 -118 -271 -128
-298 -36 -94 -38 -22 -8 281 17 169 31 345 31 391 0 65 10 90 41 107 71 38 46
147 -35 155 -85 8 -124 -55 -78 -126 28 -43 25 -74 -50 -403 -106 -463 -109
-471 -126 -287 -8 83 -27 280 -43 438 -26 256 -26 292 1 322 82 91 -32 215
-119 129z m284 -1275 c102 -102 -241 -174 -426 -90 -85 39 -53 104 61 122 117
20 332 0 365 -32z"/>
<path d="M1853 1527 c-7 -8 -13 -23 -13 -35 0 -14 6 -13 16 2 9 15 81 27 185
31 l169 7 -172 4 c-94 2 -177 -2 -185 -9z"/>
<path d="M3100 1494 c0 -59 35 -94 94 -94 25 0 46 -10 46 -23 0 -13 6 -18 13
-11 23 23 -36 74 -85 74 -34 0 -48 11 -48 39 0 36 17 40 185 46 l185 7 -195 4
c-191 4 -195 3 -195 -42z"/>
<path d="M4084 1525 c96 -4 258 -4 360 0 102 4 24 7 -174 7 -198 0 -282 -3
-186 -7z"/>
<path d="M4846 1525 c52 -4 133 -4 180 0 46 5 3 8 -96 8 -99 0 -137 -4 -84 -8z"/>
<path d="M5345 1525 c41 -4 109 -4 150 0 41 5 8 8 -75 8 -82 0 -116 -3 -75 -8z"/>
<path d="M2962 1427 c356 -383 27 -1008 -430 -817 -98 42 -91 21 11 -32 123
-62 305 -48 420 33 281 197 249 735 -51 893 -23 12 -1 -23 50 -77z"/>
<path d="M3540 1480 c0 -29 -13 -40 -49 -40 -102 0 -111 -33 -111 -411 0 -266
6 -349 25 -349 19 0 25 81 25 341 0 365 4 378 105 379 37 0 45 11 45 60 0 33
-9 60 -20 60 -11 0 -20 -18 -20 -40z"/>
<path d="M4640 1366 c0 -96 -9 -167 -24 -182 -19 -19 -12 -24 31 -24 l55 0 -6
173 c-8 235 -56 263 -56 33z"/>
<path d="M5160 1481 c0 -27 -13 -41 -39 -41 -83 0 -70 -60 44 -206 81 -103 96
-78 17 29 -72 98 -78 137 -22 137 43 0 59 86 20 110 -11 7 -20 -6 -20 -29z"/>
<path d="M5627 1486 c-5 -21 -35 -48 -66 -59 -54 -21 -222 -210 -265 -300 -22
-45 234 -427 285 -427 24 0 -5 43 -157 239 -114 146 -115 132 30 311 63 78
119 127 161 139 48 14 65 31 65 64 0 58 -40 83 -53 33z"/>
<path d="M2260 1470 c0 -16 -17 -30 -37 -30 -92 0 -103 -48 -103 -462 0 -499
-32 -566 -256 -531 -68 10 -71 9 -35 -18 47 -35 150 -38 222 -5 104 48 118
109 126 546 6 418 9 429 98 430 35 0 45 12 45 50 0 31 -11 50 -30 50 -16 0
-30 -13 -30 -30z"/>
<path d="M2578 1414 c-141 -95 -207 -316 -151 -500 33 -107 121 -234 162 -234
16 0 6 23 -27 58 -250 268 6 847 279 632 68 -54 68 -54 -6 18 -88 86 -158 93
-257 26z"/>
<path d="M1870 1420 c7 -11 34 -20 61 -20 27 0 49 -10 49 -23 0 -13 6 -18 13
-11 23 23 -37 74 -87 74 -27 0 -43 -9 -36 -20z"/>
<path d="M3924 1429 c21 -20 136 -46 136 -30 0 9 -14 22 -32 29 -39 15 -119
16 -104 1z"/>
<path d="M4193 1427 c-29 -30 -14 -307 17 -307 24 0 30 29 30 140 l0 140 145
0 c101 1 136 6 115 20 -34 22 -286 27 -307 7z"/>
<path d="M4959 1275 c47 -63 90 -115 95 -115 13 0 -118 179 -152 206 -16 13 9
-28 57 -91z"/>
<path d="M4458 1285 c-1 -8 0 -111 2 -228 2 -133 -5 -218 -19 -226 -35 -22
-24 -51 19 -51 38 0 40 13 40 260 0 160 -8 260 -20 260 -11 0 -21 -7 -22 -15z"/>
<path d="M4193 1027 c-30 -30 -14 -347 17 -347 24 0 30 31 30 160 l0 160 65 0
c49 1 58 5 35 20 -34 22 -127 26 -147 7z"/>
<path d="M5134 915 c-91 -122 -118 -175 -107 -205 16 -40 53 -38 53 2 0 18 32
76 70 128 39 52 70 105 70 118 0 41 -38 22 -86 -43z"/>
<path d="M3800 781 l0 -181 -330 0 c-207 0 -330 -7 -330 -20 0 -13 130 -20
350 -20 l350 0 0 189 c0 104 -9 194 -20 201 -13 8 -20 -53 -20 -169z"/>
<path d="M4640 779 l0 -179 -340 0 c-213 0 -340 -7 -340 -20 0 -13 137 -20
370 -20 l370 0 0 188 c0 148 -6 190 -30 199 -25 10 -30 -16 -30 -168z"/>
<path d="M1744 716 c-13 -13 -24 -33 -23 -45 0 -12 9 -5 21 14 51 88 164 -2
121 -97 -25 -53 9 -93 39 -46 64 102 -76 256 -158 174z"/>
<path d="M5120 630 c0 -24 -31 -30 -160 -30 -93 0 -160 -8 -160 -20 0 -12 77
-20 190 -20 189 0 190 0 190 50 0 31 -11 50 -30 50 -16 0 -30 -13 -30 -30z"/>
<path d="M5660 630 c0 -24 -33 -30 -180 -30 -107 0 -180 -8 -180 -20 0 -12 83
-20 210 -20 l210 0 0 50 c0 31 -11 50 -30 50 -16 0 -30 -13 -30 -30z"/>
</g>
</svg>
		                                        </fo:instream-foreign-object>
                                    </fo:block>		
				
                    <fo:block font-size="200%">
                        Rechnung
                    </fo:block>	
					<br /> 
					<br /> 
                    <fo:block>
                        Kunde: <xsl:value-of select="kundenname"/>
                    </fo:block>
	                    <fo:block>
                        Ankunft: <xsl:value-of select="ankunft"/>
                    </fo:block>
                    <fo:block>
                        Abreise: <xsl:value-of select="abreise"/>
                    </fo:block>				
                    <fo:block>
                        Zu bezahlen bis: <xsl:value-of select="bezahlen_bis"/>
                    </fo:block>				
                    <fo:block>
                        Bezahlt am: <xsl:value-of select="bezahlt_am"/>
                    </fo:block>
                    <br /> 
                    <br /> 
                    <fo:block font-size="150%">Leistungen</fo:block>
					<fo:table table-layout="fixed" width="100%" font-size="10pt" border-color="black" border-width="0.35mm" border-style="solid" text-align="center" display-align="center" space-after="5mm">
						<fo:table-column column-width="proportional-column-width(25)"/>
						<fo:table-column column-width="proportional-column-width(25)"/>
						<fo:table-column column-width="proportional-column-width(25)"/>
                                                <fo:table-column column-width="proportional-column-width(25)"/>
                                                <fo:table-column column-width="proportional-column-width(25)"/>
						<fo:table-column column-width="proportional-column-width(25)"/>
						<fo:table-body font-size="95%">
							<fo:table-row height="8mm">
								<fo:table-cell>
									<fo:block>Leistung</fo:block>
								</fo:table-cell>
								<fo:table-cell>
									<fo:block>Mitarbeiter</fo:block>
								</fo:table-cell>
								<fo:table-cell>
									<fo:block>Anzahl</fo:block>
								</fo:table-cell>
								<fo:table-cell>
									<fo:block>Preis</fo:block>
								</fo:table-cell>
                                                                <fo:table-cell>
									<fo:block>Rabatt</fo:block>
								</fo:table-cell>
                                                                <fo:table-cell>
									<fo:block>Datum</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<xsl:for-each select="position">
								<fo:table-row>
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="leistung"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="benutzer"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="anzahl"/>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block>
											<xsl:value-of select="preis"/>
										</fo:block>
									</fo:table-cell>
                                                                        <fo:table-cell>
										<fo:block>
											<xsl:value-of select="rabatt"/>
										</fo:block>
									</fo:table-cell>
                                                                        <fo:table-cell>
										<fo:block>
											<xsl:value-of select="datum"/>
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:for-each>
						</fo:table-body>
					</fo:table>
					<fo:block>
                        Totalbetrag: <xsl:value-of select="betrag"/>
                    </fo:block>
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>
</xsl:stylesheet>
