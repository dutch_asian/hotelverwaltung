/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotelverwaltung;

import data.Kunde;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author 5im13allau
 */
@ManagedBean(name = "addBuchung")
@SessionScoped
public class Buchung {

    Hotelverwaltung hv;
    private String kundenname;
    private String kundenvorname;
    private String ankunft;
    private String abreise;
    private ArrayList names;
    private String kunde;

    public Buchung() {
        hv = new Hotelverwaltung();
        initNames();
    }

    public List<Kunde> kList() {
        Session session = HibernateUtil.getInstance().openSession();
        List<Kunde> kunden = session.createQuery("FROM Kunde").list();
        session.close();
        return kunden;
    }

    public void initNames() {
        setNames(new ArrayList<>());
        for (Kunde k : kList()) {
            getNames().add(k.getVorname() + " " + k.getName());
        }
    }

    public void splitName() {
        String[] parts = getKunde().split("\\s+");
        setKundenvorname(parts[0]);
        setKundenname(parts[1]);
    }

    public void addBuchung() throws ParseException {
        Session session = HibernateUtil.getInstance().openSession();
        Transaction tr = session.beginTransaction();
        List<Kunde> kunden = session.createQuery("FROM Kunde").list();
        DateFormat format = new SimpleDateFormat("dd-MM-yyyy");

        Date dateBegin = format.parse(getAnkunft());
        Date dateEnd = format.parse(getAbreise());
        splitName();
        
        for (Kunde k : kunden) {
            if (k.getName().equals(getKundenname()) && k.getVorname().equals(getKundenvorname())) {
                if (getAbreise().equals("")) {
                    session.save(hv.enterBuchung(k, dateBegin, null));
                } else {
                    session.save(hv.enterBuchung(k, dateBegin, dateEnd));
                }
            }
        }
        tr.commit();
        session.close();
    }

    /**
     * @return the kundenname
     */
    public String getKundenname() {
        return kundenname;
    }

    /**
     * @param kundenname the kundenname to set
     */
    public void setKundenname(String kundenname) {
        this.kundenname = kundenname;
    }

    /**
     * @return the kundenvorname
     */
    public String getKundenvorname() {
        return kundenvorname;
    }

    /**
     * @param kundenvorname the kundenvorname to set
     */
    public void setKundenvorname(String kundenvorname) {
        this.kundenvorname = kundenvorname;
    }

    /**
     * @return the ankunft
     */
    public String getAnkunft() {
        return ankunft;
    }

    /**
     * @param ankunft the ankunft to set
     */
    public void setAnkunft(String ankunft) {
        this.ankunft = ankunft;
    }

    /**
     * @return the abreise
     */
    public String getAbreise() {
        return abreise;
    }

    /**
     * @param abreise the abreise to set
     */
    public void setAbreise(String abreise) {
        this.abreise = abreise;
    }

    /**
     * @return the names
     */
    public ArrayList getNames() {
        return names;
    }

    /**
     * @param names the names to set
     */
    public void setNames(ArrayList names) {
        this.names = names;
    }

    /**
     * @return the kunde
     */
    public String getKunde() {
        return kunde;
    }

    /**
     * @param kunde the kunde to set
     */
    public void setKunde(String kunde) {
        this.kunde = kunde;
    }

}
