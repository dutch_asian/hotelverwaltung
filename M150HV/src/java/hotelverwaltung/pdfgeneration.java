/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotelverwaltung;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;
import static org.apache.xerces.impl.io.UTF8Reader.DEFAULT_BUFFER_SIZE;

/**
 *
 * @author 5im13jovanderzee
 */
@ManagedBean(name = "pdf")
@SessionScoped
public class pdfgeneration {
    
    private String id;
    private String xml;
    private Hotelverwaltung hv;
    private beans.ConverterBean cd;
    
    public pdfgeneration(){
    hv = new Hotelverwaltung();
    }
    
    public void generatePDF() throws Exception{
        xml = getHv().getPDFInfo(getId());
        System.out.println(xml);
        cd = new beans.ConverterBean(xml);
        OutputStream out;
        out = new FileOutputStream("E:\\test2.pdf");
        out = new BufferedOutputStream(out);
        cd.createPDF(out);
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();
        File file = new File("E:\\test2.pdf");
        BufferedInputStream input = null;
        BufferedOutputStream output = null;

        try {
            input = new BufferedInputStream(new FileInputStream(file), DEFAULT_BUFFER_SIZE);
            response.reset();
            response.setHeader("Content-Type", "application/pdf");
            response.setHeader("Content-Length", String.valueOf(file.length()));
            response.setHeader("Content-Disposition", "inline; filename=test2.pdf");
            output = new BufferedOutputStream(response.getOutputStream(), DEFAULT_BUFFER_SIZE);

            byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
            int length;
            while ((length = input.read(buffer)) > 0) {
                output.write(buffer, 0, length);
            }

            output.flush();
        } finally {
            close(output);
            close(input);
        }
        facesContext.responseComplete();
    }
    
    private static void close(Closeable resource) {
        if (resource != null) {
            try {
                resource.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the hv
     */
    public Hotelverwaltung getHv() {
        return hv;
    }

    /**
     * @param hv the hv to set
     */
    public void setHv(Hotelverwaltung hv) {
        this.hv = hv;
    }
    
}
