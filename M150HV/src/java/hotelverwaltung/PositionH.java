/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotelverwaltung;

import data.Benutzer;
import data.Kunde;
import data.Position;
import data.Buchung;
import data.Leistung;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author 5im13allau
 */
@ManagedBean(name = "addPosition")
@SessionScoped
public class PositionH {

    Hotelverwaltung hv;
    private String leistungsID;
    private String datum;
    private String anzahl;
    private String preis;
    private String rabatt;
    private String id;
    private Buchung b;

    public PositionH() {
        hv = new Hotelverwaltung();
    }

    public String searchBuchung() {
        Session session = HibernateUtil.getInstance().openSession();
        Transaction tr = session.beginTransaction();
        List<Buchung> buchungen = session.createQuery("FROM Buchung where BuchungID =" + id).list();
        DateFormat format = new SimpleDateFormat("dd-MM-yyyy");

        setB(buchungen.get(0));

        tr.commit();
        session.close();
        return null;
    }

    public void addPosition() throws ParseException {
        Session session = HibernateUtil.getInstance().openSession();
        Transaction tr = session.beginTransaction();
        List<Buchung> buchungen = session.createQuery("FROM Buchung where BuchungID =" + getId()).list();
        List<Leistung> leistungen = session.createQuery("FROM Leistung where LeistungID =" + getLeistungsID()).list();
        List<Benutzer> benutzer = session.createQuery("FROM Benutzer where userID = 2").list();

        DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        Date dateNow = format.parse(getDatum());
        int tAnzahl = Integer.parseInt(getAnzahl());

        session.save(hv.enterPosition(buchungen.get(0), leistungen.get(0), benutzer.get(0), tAnzahl, new BigDecimal(leistungen.get(0).getPreis() * tAnzahl), new BigDecimal(Double.parseDouble(getRabatt())), dateNow));
        tr.commit();
        session.close();
    }

    /**
     * @return the leistungsID
     */
    public String getLeistungsID() {
        return leistungsID;
    }

    /**
     * @param leistungsID the leistungsID to set
     */
    public void setLeistungsID(String leistungsID) {
        this.leistungsID = leistungsID;
    }

    /**
     * @return the datum
     */
    public String getDatum() {
        return datum;
    }

    /**
     * @param datum the datum to set
     */
    public void setDatum(String datum) {
        this.datum = datum;
    }

    /**
     * @return the anzahl
     */
    public String getAnzahl() {
        return anzahl;
    }

    /**
     * @param anzahl the anzahl to set
     */
    public void setAnzahl(String anzahl) {
        this.anzahl = anzahl;
    }

    /**
     * @return the preis
     */
    public String getPreis() {
        return preis;
    }

    /**
     * @param preis the preis to set
     */
    public void setPreis(String preis) {
        this.preis = preis;
    }

    /**
     * @return the rabatt
     */
    public String getRabatt() {
        return rabatt;
    }

    /**
     * @param rabatt the rabatt to set
     */
    public void setRabatt(String rabatt) {
        this.rabatt = rabatt;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the b
     */
    public Buchung getB() {
        return b;
    }

    /**
     * @param b the b to set
     */
    public void setB(Buchung b) {
        this.b = b;
    }

}
